package Shammer;

        import javafx.event.ActionEvent;
        import javafx.event.EventHandler;
        import javafx.fxml.FXML;
        import javafx.fxml.Initializable;
        import javafx.scene.control.Button;
        import javafx.scene.control.Label;
        import javafx.scene.control.TextField;
        import javafx.scene.image.Image;
        import javafx.scene.image.ImageView;

        import java.net.URL;
        import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    public Button fetchButton;
    public Label degreeWeather;
    public Label cityState;
    public Label typeWeather;
    public TextField zipCode;
    public ImageView weatherIcon;
    public Button convertButton;
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }


    public void buttonPress(ActionEvent actionEvent) {
        // boolean switch = true;
        Weather w = new Weather(zipCode.getText());
        w.fetch();


        //w.getCityState();
        cityState.setText(w.getCityState());

        //w.getTemp();
        //degreeWeather.setText("" + w.getTempF() + "F");

        //degreeWeather.setText("" + w.getTempC() + "C");

        String T = degreeWeather.getText();

        degreeWeather.setText("" + w.getTempF() + "F");

        convertButton.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        String T = degreeWeather.getText();


                        if (T.charAt(T.length() - 1) == 'F') {
                            degreeWeather.setText("" + w.getTempC() + "C");
                        } else {
                            degreeWeather.setText("" + w.getTempF() + "F");
                        }
                    }
                }
        );



        String currentWeather = w.getWeather();
        typeWeather.setText(currentWeather);
        try {
            //Image weatherImage = new Image("file:resources/icons/" + currentWeather.toLowerCase() + ".png");
            Image weatherImage = new Image("file:resources/icons/" + w.getIcon());
            weatherIcon.setImage(weatherImage);
        } catch(IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

}
