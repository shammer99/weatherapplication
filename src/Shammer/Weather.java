package Shammer;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import javafx.scene.Scene;

public class Weather {
    private String zip;

    JsonElement jse;

    public Weather(String zip)
    {
        this.zip = zip;
    }

    public void fetch() {
        try {
                              //put Aeris key
            String urlString = "https://api.aerisapi.com/observations/" +URLEncoder.encode(zip, "utf-8" ) + "?client_id=CF0efkpGXmI6VjS5CP7wO&client_secret=qQqWhVMgh9MJNkSjDk0XTwkc25Ps9TrwNz6wSGHN";
                    //"wQhXMMnxoRV4HNKoRLZrL" + URLEncoder.encode(zip, "utf-8" )+ "rUOW0GEyf5bT9JhUzro2WQAuUpj3A7nFHgVCRGEK";

            URL weatherURL = new URL(urlString);

            InputStream is = weatherURL.openStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            jse = JsonParser.parseReader(br);
            System.out.println(jse);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public int getTempF()
    {
        return jse.getAsJsonObject().get ("response").getAsJsonObject().get("ob").getAsJsonObject().get("tempF").getAsInt();
    }
    public int getTempC()
    {
        return jse.getAsJsonObject().get ("response").getAsJsonObject().get("ob").getAsJsonObject().get("tempC").getAsInt();
    }
    public String getWeather()
    {
        return jse.getAsJsonObject().get ("response").getAsJsonObject().get("ob").getAsJsonObject().get("weather").getAsString();
    }
    public String getCityState()
    {
        return jse.getAsJsonObject().get ("response").getAsJsonObject().get("place").getAsJsonObject().get("name").getAsString() + ", " + jse.getAsJsonObject().get ("response").getAsJsonObject().get("place").getAsJsonObject().get("state").getAsString().toUpperCase();
    }
    public String getIcon()
    {
        return jse.getAsJsonObject().
            get("response").getAsJsonObject().
            get("ob").getAsJsonObject().
            get("icon").getAsString();
    }



    /*
    public static void main(String[] args)
    {
        Weather w = new Weather("95677");
        w.fetch();
        System.out.println(w.getTemp());
    }

     */
}
