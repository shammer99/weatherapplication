module SimpleWeather {
    requires javafx.controls;
    requires javafx.media;
    requires javafx.base;
    requires javafx.fxml;
    requires com.google.gson;
    requires javafx.graphics;
    opens Shammer;
}